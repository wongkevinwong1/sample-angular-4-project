///<reference path="../../../node_modules/@angular/core/src/metadata/lifecycle_hooks.d.ts"/>
import {Component, OnInit} from '@angular/core';
import {IProduct} from './product';
import {ProductService} from "./product.service";

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent
implements  OnInit {


  pageTitle: string = 'Product List';
  imageWidth: number = 100;
  imageMargin: number = 2;
  errorMessage: string;
  showImage: boolean = false;
  _listFilter:string;

  get listFilter():string{
    return this._listFilter;
  }

  set listFilter(value:string){
    this._listFilter = value;
    this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.products;
  }

  constructor(private _productService : ProductService){
  }

  filteredProducts: IProduct[];
  products: IProduct[];

  toggleImage(): void {
    this.showImage = !this.showImage;
  }

  ngOnInit(): void {
    this._productService.getProducts().subscribe(
      products => {
        this.products = products;

        this.filteredProducts = this.products;
      },
          error=> this.errorMessage = <any>error
    );
  }

  performFilter(filterBy: string) : IProduct[] {
    filterBy = filterBy.toLocaleLowerCase();

    return this.products.filter((product:IProduct)=>{
      return product.productName.toLocaleLowerCase().indexOf(filterBy) !== -1;
    });
  }

  onRatingClicked (message:string): void {
    this.pageTitle = 'Product List : ' + message;
  }
}
