///<reference path="../../../node_modules/@angular/router/src/interfaces.d.ts"/>
import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from "@angular/router";

@Injectable()
export class ProductGuardService implements CanActivate{
  canActivate(route: ActivatedRouteSnapshot): boolean  {
    let id= +route.url[1].path;

    if(isNaN(id) || id < 1){
      this._router.navigate(['/products']);
      alert('invalid product id');
      return false;
    } else {
      return true;
    }
  }

  constructor(private _router: Router) { }

}
