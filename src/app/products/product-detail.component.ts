///<reference path="../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import { Component, OnInit } from '@angular/core';
import {IProduct} from "./product";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {ProductService} from "./product.service";

@Component({
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  pageTitle: string = 'Product Detail';
  product: IProduct;

  constructor(private _route: ActivatedRoute, private _productService: ProductService, private _router: Router) {
    console.log(this._route.snapshot.paramMap.get('id'));
  }

  ngOnInit() {
    let id= +this._route.snapshot.paramMap.get('id');

    this.pageTitle += ` : ${id}`;

    this._productService.getProduct(id)
      .subscribe(product=>this.product = product, error=> alert(<any>error));

  }

  onBackButtonClicked():void {
    this._router.navigate(['/products']);
  }
}
