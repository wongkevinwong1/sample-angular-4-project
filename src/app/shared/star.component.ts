import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';

@Component({
  selector: 'pm-star',
  templateUrl: './star.component.html',
  styleUrls : ['./star.component.css']
})
export class StarComponent implements OnChanges {
  ngOnChanges(): void {
    this.starWidth = this.rating * 86/5;
  }

  onClick(): void {
    this.ratingClicked.emit(`Selected product has rating ${this.rating}`);
  }

  @Input() rating:number;
  starWidth:number;

  @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();
}
